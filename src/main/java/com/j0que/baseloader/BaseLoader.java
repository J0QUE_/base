package com.j0que.baseloader;

import com.j0que.baseloader.util.Logging;
import com.j0que.baseloader.util.Version;

/**
 * Created by J0QUE on 24-12-2014.
 */

@Version(majorVersion = 1, minorVersion = 0, build = 0)
public class BaseLoader {

    public static LoaderBase loaderBase = new LoaderBase();

    public static void main(String[] args) {
        loaderBase.Load();
        Logging.logLoaderState();

    }

}
