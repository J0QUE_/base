package com.j0que.baseloader;

import com.j0que.baseloader.util.EnumLoaderState;
import com.j0que.baseloader.util.ILoaderBase;

/**
 * Created by J0QUE on 24-12-2014.
 */

public class LoaderBase implements ILoaderBase {

    private EnumLoaderState state;
    private Class Loader;

    @Override
    public void Load() {
        this.state = EnumLoaderState.LOADING;
        this.Loader = LoaderBase();
    }

    @Override
    public Class LoaderBase() {
        return LoaderBase.class;
    }

    @Override
    public EnumLoaderState LoaderState() {
        return state;
    }
}
