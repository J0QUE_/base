package com.j0que.baseloader.util;

/**
 * Created by J0QUE on 24-12-2014.
 */

public enum EnumLoaderState {


    /**
     * Downloading Files
     */
    DOWNLOADING,

    /**
     * Booting the loaded file
     * Most of the time a jar file
     */
    BOOTING,

    /**
     * Loading the loader itself
     */
    LOADING,

    /**
     * Ready
     */
    READY

}
