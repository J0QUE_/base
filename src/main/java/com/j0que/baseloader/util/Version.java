package com.j0que.baseloader.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by J0QUE on 24-12-2014.
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Version {
    int majorVersion() default 1;
    int minorVersion() default 0;
    int build() default 0;
}
