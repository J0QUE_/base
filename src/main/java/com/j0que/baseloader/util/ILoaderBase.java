package com.j0que.baseloader.util;

/**
 * Created by J0QUE on 24-12-2014.
 */

public interface ILoaderBase {

    void Load();

    Class LoaderBase();

    EnumLoaderState LoaderState();



}
