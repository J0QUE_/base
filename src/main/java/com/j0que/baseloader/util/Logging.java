package com.j0que.baseloader.util;

import com.j0que.baseloader.BaseLoader;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by J0QUE on 24-12-2014.
 */

public class Logging {

    private final static Logger LOGGING = Logger.getLogger(BaseLoader.class.getName());

    public static void log(Level level, String message) {
        LOGGING.log(level, message);
    }

    public static void info(String message) {
        log(Level.INFO, message);
    }

    public static void logLoaderState() {
        info("LoaderState: "+BaseLoader.loaderBase.LoaderState().toString());
    }



}
